# {{Developing World}}

## User Driven

Users discover and invent technologies by defeating one-time dungeons, experimenting, etc.

An example of this would be discovering how to make an object fly in the world which could lead to the invention of the Airship.

Users must learn about a technology before being able to utilize it.

## Events

Events can be beneficial or destructive.
