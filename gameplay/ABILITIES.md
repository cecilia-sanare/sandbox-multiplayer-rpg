# {{Skills}}

### {{Artist}}

* Art would be drawn by the player
* Should support drawing tablets
* Paintings, Murals, etc.

### {{Architect}}

* Anyone can participate in the construction of a building, but it takes an architect to design it
  - Structures should take a substantial (semi-realistic) amount of time to construct
* in-depth designing process that imitates what a real life architect would go through
* The design of a structure affects certain things, such as how it will react to an attack.

### {{Tailoring}}

### Smithing / Metalworking

### Foraging

### Mining

### Fishing
