# Player

## Permadeath

Players should feel like they’re responsible for their actions. What they do could possibly cause another player to kill them.

## World PvP

PvP is allowed **ANYWHERE**.

## No/Limited Humanoid NPCs
